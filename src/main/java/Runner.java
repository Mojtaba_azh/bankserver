/*
 * This is the main class for Running Server application.
 *
 * author: Mojtaba Azhdari
 */
import server.CentralServer;

public class Runner {

    // **************************************************
    // Fields
    // **************************************************
    private static final int _SERVER_PORT_NUMBER = 9595;

    public static void main(String [] args) throws Exception {
        CentralServer server = new CentralServer(_SERVER_PORT_NUMBER);
        server.setUp();
    }
}
