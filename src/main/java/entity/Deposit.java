package entity;

/*
 * This class is used for defining Deposits.
 *
 * author: Mojtaba Azhdari
 */
public class Deposit {

    // **************************************************
    // Fields
    // **************************************************
    private String depositId;
    private String customerName;
    private String initialBalance;
    private String upperBound;

    // **************************************************
    // Constructor
    // **************************************************

    public Deposit(String depositId, String customerName, String initialBalance, String upperBound) {
        this.depositId = depositId;
        this.customerName = customerName;
        this.initialBalance = initialBalance;
        this.upperBound = upperBound;
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public String getDepositId() {
        return depositId;
    }

    public String getInitialBalance() {
        return initialBalance;
    }

    public String getUpperBound() {
        return upperBound;
    }

    // **************************************************
    // Setter methods
    // **************************************************
    public void setInitialBalance(String initialBalance) {
        this.initialBalance = initialBalance;
    }

}
