package entity;
/*
 * This class is used for defining Responses of Transactions.
 *
 * author: Mojtaba Azhdari
 */

import java.io.Serializable;

public class TransactionResponse implements Serializable {
    private static final long serialVersionUID = 7232659830023086970L;

    // **************************************************
    // Fields
    // **************************************************
    private final String transactionId;
    private final String depositId;
    private final boolean result;
    private final String remainingBalance;
    private final String description;

    // **************************************************
    // Constructor for failed transaction response
    // **************************************************
    public TransactionResponse(boolean result, String transactionId, String depositId, String description, String remainingBalance) {
        this.result = result;
        this.transactionId = transactionId;
        this.depositId = depositId;
        this.description = description;
        this.remainingBalance = remainingBalance;
    }

    // **************************************************
    // Constructor for failed transaction response without remainingBalance and depositId
    // **************************************************
    public TransactionResponse(boolean result, String transactionId, String description) {
        this(result, transactionId, "", description, "");
    }

    // **************************************************
    // Constructor for successful transaction response
    // **************************************************
    public TransactionResponse(String transactionId, String depositId, String remainingBalance) {
        this(true, transactionId, depositId, "", remainingBalance);
    }

    public String getDescription() {
        return description;
    }
}
