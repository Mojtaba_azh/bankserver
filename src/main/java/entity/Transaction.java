package entity;
/*
 * This class is used for defining Transactions.
 *
 * author: Mojtaba Azhdari
 */

import java.io.Serializable;

public class Transaction implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;

    // **************************************************
    // Fields
    // **************************************************
    private final String transactionId;
    private final TransactionType transactionType;
    private final String amount;
    private final String depositId;

    // **************************************************
    // Constructor
    // **************************************************
    public Transaction(String transactionId, TransactionType transactionType, String amount, String depositId) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.amount = amount;
        this.depositId = depositId;
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public String getTransactionId() {
        return transactionId;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public String getAmount() {
        return amount;
    }

    public String getDepositId() {
        return depositId;
    }

}
