package server;
/*
 * This class is used for managing Terminal Clients that want connect to server.
 *
 * author: Mojtaba Azhdari
 */

import entity.*;
import utility.*;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.Map;

public class TerminalManager implements Runnable {

    // **************************************************
    // Fields
    // **************************************************
    private final ObjectInputStream objectInputStream;
    private final ObjectOutputStream objectOutputStream;
    private final DataOutputStream dataOutputStream;
    private final Socket socket;

    // **************************************************
    // Constructor
    // **************************************************
    TerminalManager(Socket socket, ObjectInputStream objectInputStream, ObjectOutputStream objectOutputStream, DataOutputStream dataOutputStream) {
        this.socket = socket;
        this.objectInputStream = objectInputStream;
        this.objectOutputStream = objectOutputStream;
        this.dataOutputStream = dataOutputStream;
    }

    @Override
    public void run() {
        int numberOfCommittedTransaction = 0;
        while (true) {
            TransactionResponse transactionResponseFail = null;
            try {
                Transaction transaction = (Transaction) objectInputStream.readObject();
                LoggingUtility.writeLog(Thread.currentThread().getName());
                transactionResponseFail = Validation.validateInputFormat(transaction);
                if (transactionResponseFail != null) {
                    throw new Exception(transactionResponseFail.getDescription());
                }
                synchronized (this) {
                    transactionResponseFail = Validation.validateDepositInformation(transaction);
                    if (transactionResponseFail != null) {
                        throw new Exception(transactionResponseFail.getDescription());
                    }
                    TransactionResponse transactionResponseSuccess = processDeposit(transaction);
                    dataOutputStream.writeUTF("Transaction " + transaction.getTransactionId() + ", successfully performed. :D");
                    objectOutputStream.writeObject(transactionResponseSuccess);
                    LoggingUtility.writeLog("Transaction " + transaction.getTransactionId() + ", successfully performed. :D");
                    numberOfCommittedTransaction++;
                }
            } catch (IOException e) {
                LoggingUtility.writeLog(numberOfCommittedTransaction + " Transaction performed. Good bye");
                LoggingUtility.writeLog("Waiting for new Terminals...");
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            } catch (Exception e) {
                LoggingUtility.writeLog(e.getMessage());
                try {
                    dataOutputStream.writeUTF(e.getMessage());
                    objectOutputStream.writeObject(transactionResponseFail);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private TransactionResponse processDeposit(Transaction transaction) throws Exception {
        String transactionId = transaction.getTransactionId();
        TransactionType transactionType = transaction.getTransactionType();
        String amount = transaction.getAmount();
        String depositId = transaction.getDepositId();
        Map<String, Deposit> depositMap = JsonUtility.parseDeposits();
        Deposit deposit = depositMap.get(depositId);
        BigDecimal balance = new BigDecimal(deposit.getInitialBalance());
        BigDecimal amountBD = new BigDecimal(amount);
        if (transactionType == TransactionType.DEPOSIT) {
            deposit.setInitialBalance(String.valueOf(balance.add(amountBD)));
        } else {
            deposit.setInitialBalance(String.valueOf(balance.subtract(amountBD)));
        }
        depositMap.replace(depositId, deposit);
        JsonUtility.updateDepositBalance(deposit);
        return new TransactionResponse(transactionId, depositId, deposit.getInitialBalance());
    }
}
