package server;
/*
 * This class is Server Class.
 *
 * author: Mojtaba Azhdari
 */

import utility.JsonUtility;
import utility.LoggingUtility;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CentralServer {

    // **************************************************
    // Fields
    // **************************************************
    private ServerSocket serverSocket;
    private static final int _SERVER_SOCKET_TIMEOUT = 30000;

    // **************************************************
    // Constructor
    // **************************************************
    public CentralServer(int portInRunner) throws Exception {
        JsonUtility.setServerPortNumber(portInRunner);
        int portInCore = JsonUtility.getServerPortNumber();
        serverSocket = new ServerSocket(portInCore);
        serverSocket.setSoTimeout(_SERVER_SOCKET_TIMEOUT);
    }

    // **************************************************
    // This method is used for setting up server and accepting terminal clients
    // **************************************************
    public void setUp() throws IOException {
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        while (true) {
            Socket socket = null;
            try {
                LoggingUtility.writeLog("Waiting for Terminal on port " + serverSocket.getLocalPort() + "...");
                socket = serverSocket.accept();
                LoggingUtility.writeLog("Just connected to " + socket.getRemoteSocketAddress() + "\t:D");
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                LoggingUtility.writeLog("Assigning new thread for this client");
                threadPool.execute(new TerminalManager(socket, objectInputStream, objectOutputStream, dataOutputStream));
            } catch (SocketTimeoutException e) {
                LoggingUtility.writeLog("Server disconnected.");
                threadPool.shutdown();
                if (socket != null)
                    socket.close();
                break;
            } catch (Exception e) {
                e.printStackTrace();
                if (socket != null)
                    socket.close();
            }
        }
        System.exit(0);
    }
}
