package utility;
/*
 * This class is used for defining custom messages for all exceptions
 *
 * author: Mojtaba Azhdari
 */

import java.util.HashMap;
import java.util.Map;

abstract class ExceptionMessages {

    // **************************************************
    // Fields
    // **************************************************
    private static Map<String, String> messages;

    static {
        messages = new HashMap<>();
        messages.put("transactionId", "ERROR-1 * WRONG TRANSACTION_ID     :  The value of id in your terminal.xml file, should contain only digits (0-9) !");
        messages.put("amount", "ERROR-2 * WRONG AMOUNT             :  The value of amount in your terminal.xml file, should contain only digits (0-9) !");
        messages.put("depositId", "ERROR-3 * WRONG DEPOSIT_ID         :  The value of deposit in your terminal.xml file, should contain only digits (0-9) !");
        messages.put("transactionType", "ERROR-4 * WRONG TRANSACTION_TYPE   :  The value of type in your terminal.xml file, should be \'deposit\' or \'withdraw\' !");
        messages.put("customerNotFound", "ERROR-5 * WRONG CUSTOMER           :  The customer with id in your terminal.xml file, is not exist in the server !");
        messages.put("amountGreaterThanUpperBound", "ERROR-6 * WRONG AMOUNT             :  The amount of you transaction is greater than the upperBound of this account !");
        messages.put("amountGreaterThanBalance", "ERROR-7 * WRONG AMOUNT             :  This account's balance is not enough for this transaction !");
    }

    static String getMessage(String exceptionMessageName) {
        return messages.get(exceptionMessageName);
    }
}
