package utility;
/*
 * This class is used for defining read and write data to JSON file
 *
 * author: Mojtaba Azhdari
 */
import entity.Deposit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class JsonUtility {

    // **************************************************
    // Fields
    // **************************************************
    private static final String _SERVER_CONFIG_FILE = "json-file/core.json";

    public static void setServerPortNumber(int serverPortNumber) throws IOException, ParseException {
        JSONObject jsonObject = readFromFile();
        jsonObject.replace("port",serverPortNumber);
        writeToFile(jsonObject);
    }

    public static void updateDepositBalance(Deposit deposit) throws IOException, ParseException {
        JSONObject jsonObject = readFromFile();
        JSONArray deposits = (JSONArray) jsonObject.get("deposits");
        for (Object depositObject : deposits) {
            JSONObject depositToEdit = (JSONObject) depositObject;
            if(depositToEdit.get("id").toString().equals(deposit.getDepositId())) {
                depositToEdit.replace("initialBalance",deposit.getInitialBalance());
                break;
            }
        }
        writeToFile(jsonObject);
    }

    private static JSONObject readFromFile() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(new FileReader(_SERVER_CONFIG_FILE));
    }

    private static void writeToFile(JSONObject jsonObject) throws IOException {
        FileWriter jsonWriter = new FileWriter(_SERVER_CONFIG_FILE);
        jsonWriter.write(jsonObject.toJSONString());
        jsonWriter.close();
    }

    public static int getServerPortNumber() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(_SERVER_CONFIG_FILE));
        long serverPortNumber = (Long) jsonObject.get("port");
        return (int) serverPortNumber;
    }

    public static Map<String,Deposit> parseDeposits() throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(_SERVER_CONFIG_FILE));
        JSONArray depositJsonArray = (JSONArray) jsonObject.get("deposits");
        Map <String , Deposit> depositMap = new HashMap<>();
        JSONObject depositJsonObject;
        for (Object depositObject : depositJsonArray) {
            depositJsonObject = (JSONObject) depositObject;
            String depositId = depositJsonObject.get("id")+"";
            String customerName = depositJsonObject.get("customer")+"";
            String initialBalance = depositJsonObject.get("initialBalance")+"";
            String upperBound = depositJsonObject.get("upperBound")+"";
            Deposit deposit = new Deposit(depositId,customerName,initialBalance,upperBound);
            depositMap.put(depositId , deposit);
        }
        return depositMap;
    }

}
