package utility;

import entity.Deposit;
import entity.Transaction;
import entity.TransactionResponse;
import entity.TransactionType;

import java.math.BigDecimal;
import java.util.Map;

public abstract class Validation {

    public static TransactionResponse validateInputFormat(Transaction transaction) {
        String transactionId = transaction.getTransactionId();
        TransactionType transactionType = transaction.getTransactionType();
        String amount = transaction.getAmount();
        String depositId = transaction.getDepositId();
        String exceptionMessage;
        TransactionResponse transactionResponseFail = null;
        if (!(transactionId.chars().allMatch(Character::isDigit))) {
            exceptionMessage = ExceptionMessages.getMessage("transactionId");
            transactionResponseFail = new TransactionResponse(false, transactionId, exceptionMessage);
        } else if (!(amount.chars().allMatch(Character::isDigit))) {
            exceptionMessage = ExceptionMessages.getMessage("amount");
            transactionResponseFail = new TransactionResponse(false, transactionId, exceptionMessage);
        } else if (!(depositId.chars().allMatch(Character::isDigit))) {
            exceptionMessage = ExceptionMessages.getMessage("depositId");
            transactionResponseFail = new TransactionResponse(false, transactionId, exceptionMessage);
        } else if (!((transactionType == TransactionType.DEPOSIT) || (transactionType == TransactionType.WITHDRAW))) {
            exceptionMessage = ExceptionMessages.getMessage("transactionType");
            transactionResponseFail = new TransactionResponse(false, transactionId, exceptionMessage);
        }
        return transactionResponseFail;
    }

    public static TransactionResponse validateDepositInformation(Transaction transaction) throws Exception {
        Map<String, Deposit> depositMap = JsonUtility.parseDeposits();
        String transactionId = transaction.getTransactionId();
        TransactionType transactionType = transaction.getTransactionType();
        String amount = transaction.getAmount();
        String depositId = transaction.getDepositId();
        String exceptionMessage;
        TransactionResponse transactionResponseFail = null;
        if (!(depositMap.containsKey(depositId))) {
            exceptionMessage = ExceptionMessages.getMessage("customerNotFound");
            transactionResponseFail = new TransactionResponse(false, transactionId, exceptionMessage);
        }

        Deposit deposit = depositMap.get(depositId);
        BigDecimal upperBound = new BigDecimal(deposit.getUpperBound());
        BigDecimal balance = new BigDecimal(deposit.getInitialBalance());
        BigDecimal amountBD = new BigDecimal(amount);

        if (amountBD.compareTo(upperBound) > 0) {
            exceptionMessage = ExceptionMessages.getMessage("amountGreaterThanUpperBound");
            transactionResponseFail = new TransactionResponse(false, transactionId, depositId, exceptionMessage, balance + "");
        }
        if (transactionType == TransactionType.WITHDRAW && amountBD.compareTo(balance) > 0) {
            exceptionMessage = ExceptionMessages.getMessage("amountGreaterThanBalance");
            transactionResponseFail = new TransactionResponse(false, transactionId, depositId, exceptionMessage, balance + "");
        }
        return transactionResponseFail;
    }
}
